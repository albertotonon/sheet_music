fis4\downbow fis g a
a4 g fis e
d4 d e fis
fis4.\downbow (e8\downbow) e2\upbow
\break

fis4 fis g a
a4 g fis e
d4 d e fis
e4.\downbow (d8\downbow) d2\upbow
\break

e4 e fis d
e4 fis8\upbow(g8\upbow) fis4 d
e4 fis8\upbow(g8\upbow) fis4 e
d4 e a, r
\break

fis'4\downbow fis g a
a4 g fis e
d4 d e fis
e4.\downbow (d8\downbow) d2\upbow \bar "|."
