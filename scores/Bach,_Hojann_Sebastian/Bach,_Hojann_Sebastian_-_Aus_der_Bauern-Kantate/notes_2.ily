a,4 |
a'4 a a d, |
e4 e e a |
fis4 d e e |
a,2 a |
a'4 a a a |
\break

e4 e e a |
g4 a e' e, |
a2. a8 (b) |
cis4 a a a |
d4 a a cis8 (b) |
cis4 a d8 (cis) b8 (a) |
\break

e4 e8 (fis) e8 (d) cis (b) |
a4 a'8 (gis) a8 (gis) a8 (fis) |
gis4 e e a |
fis4 d e e |
a,2. a'8 (b) |
gis4 a a a |
\break

d4 a a cis8 (b) |
cis4 a d8 (cis) b8 (a) |
e4 e8 (fis) e8 (d) cis8 (b) |
a4 gis'8 (a) b8 (a) gis8 (fis) |
gis8 (fis) gis8 (a) gis4 a |
fis4 d e e |
a,2. \bar "|."
