\paper {
	#(set-default-paper-size "a4")
	left-margin = 1.5\cm
	ragged-last = ##f
	print-page-number = ##t
	print-first-page-number = ##t
	evenHeaderMarkup = \markup \null
	oddHeaderMarkup = \markup \null
	oddFooterMarkup = \markup {
		\fill-line {
			\on-the-fly \print-page-number-check-first
			\fromproperty #'page:page-number-string
		}
	}
	evenFooterMarkup = \oddFooterMarkup
}
